package com.alj.firstprj;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.view.View;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Main4Activity extends AppCompatActivity implements View.OnClickListener {
    Button B_one, B_two, B_three, B_four, B_five, B_six, B_seven, B_eight, B_nine, B_zero, B_mod, B_cec, B_clear, B_bck, B_fraction, B_square, B_sqrt, B_negative, B_add, B_subtract, B_multiply, B_divide, B_decimal, B_equal;
    TextView screen;
    int a, b, c, d, e, f, g, h, i, j, k, l,id;
    String s1, s2, s3, s4, s5, s6, s7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);
        B_one = findViewById(R.id.one);
        B_two = findViewById(R.id.two);
        B_three = findViewById(R.id.three);
        B_four = findViewById(R.id.four);
        B_five = findViewById(R.id.five);
        B_six = findViewById(R.id.six);
        B_seven = findViewById(R.id.seven);
        B_eight = findViewById(R.id.eight);
        B_nine = findViewById(R.id.nine);
        B_zero = findViewById(R.id.zero);
        B_equal = findViewById(R.id.equal);
        B_add = findViewById(R.id.add);
        B_subtract = findViewById(R.id.subtract);
        B_multiply = findViewById(R.id.multiply);
        B_divide = findViewById(R.id.divide);
        B_decimal = findViewById(R.id.decimal);
        B_mod = findViewById(R.id.mod);
        B_sqrt = findViewById(R.id.sqrt);
        B_square = findViewById(R.id.square);
        B_negative = findViewById(R.id.negative);
        B_cec = findViewById(R.id.cec);
        B_clear = findViewById(R.id.clear);
        B_bck = findViewById(R.id.bck);
        B_fraction = findViewById(R.id.fraction);
        screen = findViewById(R.id.screen);
        B_one.setOnClickListener(this);
        B_two.setOnClickListener(this);
        B_three.setOnClickListener(this);
        B_four.setOnClickListener(this);
        B_five.setOnClickListener(this);
        B_six.setOnClickListener(this);
        B_seven.setOnClickListener(this);
        B_eight.setOnClickListener(this);
        B_nine.setOnClickListener(this);
        B_zero.setOnClickListener(this);
        B_add.setOnClickListener(this);
        B_subtract.setOnClickListener(this);
        B_multiply.setOnClickListener(this);
        B_divide.setOnClickListener(this);
        B_decimal.setOnClickListener(this);
        B_equal.setOnClickListener(this);
    }

    public void onClick(View v) {
        if (v.getId() == R.id.one) {
            s1 = (String) screen.getText();
            screen.setText(s1 + "1");
        } else if (v.getId() == R.id.two) {
            s1 = (String) screen.getText();
            screen.setText(s1 + "2");
        } else if (v.getId() == R.id.three) {
            s1 = (String) screen.getText();
            screen.setText(s1 + "3");
        } else if (v.getId() == R.id.four) {
            s1 = (String) screen.getText();
            screen.setText(s1 + "4");
        } else if (v.getId() == R.id.five) {
            s1 = (String) screen.getText();
            screen.setText(s1 + "5");
        } else if (v.getId() == R.id.six) {
            s1 = (String) screen.getText();
            screen.setText(s1 + "6");
        } else if (v.getId() == R.id.seven) {
            s1 = (String) screen.getText();
            screen.setText(s1 + "7");
        } else if (v.getId() == R.id.eight) {
            s1 = (String) screen.getText();
            screen.setText(s1 + "8");
        } else if (v.getId() == R.id.nine) {
            s1 = (String) screen.getText();
            screen.setText(s1 + "9");
        } else if (v.getId() == R.id.zero) {
            s1 = (String) screen.getText();
            screen.setText(s1 + "0");
        } else if (v.getId() == R.id.add) {
            s2 = (String) screen.getText();

            screen.setText("");
            id = 1;
        }
        else if (v.getId() == R.id.subtract) {
            s2 = (String) screen.getText();

            screen.setText("");
            id = 2;
        }
        else if (v.getId() == R.id.multiply) {
            s2 = (String) screen.getText();

            screen.setText("");
            id = 3;
        }
        else if (v.getId() == R.id.divide) {
            s2 = (String) screen.getText();

            screen.setText("");
            id = 4;
        }
        else if (v.getId() == R.id.equal) {
            if (id == 1) {
                s3 = (String) screen.getText();
                a = Integer.parseInt(s2);
                b = Integer.parseInt(s3);
                c = a + b;
            }
            if (id == 2) {
                s3 = (String) screen.getText();
                a = Integer.parseInt(s2);
                b = Integer.parseInt(s3);
                c = a - b;
            }
            if (id == 3) {
                s3 = (String) screen.getText();
                a = Integer.parseInt(s2);
                b = Integer.parseInt(s3);
                c = a * b;
            }
            if (id == 4) {
                s3 = (String) screen.getText();
                a = Integer.parseInt(s2);
                b = Integer.parseInt(s3);
                 c = a / b;
            }
                screen.setText(""+c);
            }
        }
    }
