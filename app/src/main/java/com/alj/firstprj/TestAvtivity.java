package com.alj.firstprj;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

public class TestAvtivity extends AppCompatActivity {
ListView list1;
LinearLayout l2;
TextView ts;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_avtivity);
        list1=findViewById(R.id.list1);
        ts=findViewById(R.id.textT);
        l2=findViewById(R.id.kkj);
        ArrayAdapter adapter=new ArrayAdapter(this,R.layout.list_activity,getResources().getStringArray(R.array.Day));
        list1.setAdapter(adapter);
        list1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ts.setText(parent.getItemAtPosition(position).toString());
                Toast.makeText(TestAvtivity.this, parent.getItemAtPosition(position).toString(),Toast.LENGTH_SHORT).show();
                Snackbar.make(l2,"Done",Snackbar.LENGTH_SHORT).show();
                showDialog();
            }
            private void showDialog(){
                AlertDialog.Builder builder=new AlertDialog.Builder(TestAvtivity.this);
                builder.setTitle("Alert").setMessage("Hello.....");
                builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(TestAvtivity.this,"Contine",Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                }).show();
            }
        });
    }
}
